const unicitate = (value, index, self) => {
	return self.indexOf(value) === index
  }

function distance(first, second){
	let distanta = 0;
	if (Array.isArray(first) && Array.isArray(second))
	{
		if (first.length < 1 || second.length < 1)
	{
		distanta = 0;
	}
	else
	{
		first.sort();
		second.sort();
		let Dist1 = first.filter(unicitate);
		let Dist2 = second.filter(unicitate);
		let Dist = Dist1.concat(Dist2);
		Dist.sort();
		let Dist3 = Dist.filter(unicitate);
		if(Dist1.length+Dist2.length==Dist3.length)
			distanta = Dist3.length;
		else
		{
			if(Dist1.length>Dist2.length)
			{
				distanta = Dist3.length-Dist2.length;
			}
			else
			{
				distanta = Dist3.length-Dist1.length;
			}
		}
	}
	}
	else
	{
		let x = new Error("InvalidType");
		throw x;
	}
	return distanta;
}


module.exports.distance = distance